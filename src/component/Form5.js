import React from 'react';

const Form5 = (props) => {
    const {origine, transport, voyage, info}= props

    return (
    <div>
    
        <div>
            <h1> D'où venez-vous?</h1>
    <p> {origine}</p>
        </div>
        <div>
        <h1>Quel est votre moyen de locomotion préféré?</h1>
        <p> {transport}</p>
        </div>
        <div>
        <h1> Quels sont les prochains pays que vous pensez visiter?</h1>
        <p> {voyage.map((value)=> {return <span key={value}>{value}</span >})}</p>
        </div>
        <div>
        <h1> Vos infos:</h1>
    <p>Prénom: {info.prenom}</p>
    <p>Nom: {info.nom}</p>
    <p>Adresse: {info.adresse}</p>
    <p>Mail: {info.mail}</p>
    <p>Pays: {info.pays}</p>
        </div>
    </div>
    );
}

export default Form5;