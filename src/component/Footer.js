import React from 'react';

const Footer = (props) => {
const {currentPage, setCurrentPage}= props;


function handleClick(){
    if(currentPage<5){
        setCurrentPage(currentPage +1)
    }
    

}

    return (
        <div>
            <div>
                <div className= {currentPage===1 ? "page active":"page"}>
                    
                </div>
                <div className= {currentPage===2 ? "page active":"page"}>
                    
                </div>
                <div className= {currentPage===3 ? "page active":"page"}>
                    
                </div>
                 <div className= {currentPage===4 ? "page active":"page"}>
                    
                </div>
                <div className= {currentPage===5 ? "page active":"page"}>
                    
                </div>
            </div>
            <div>
                <button onClick= {handleClick}> Next</button>
            </div>
        </div>
    );
}

export default Footer;