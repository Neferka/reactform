import React from 'react';
import bike from "../images/bike.jpeg"
import bus from "../images/bus.jpeg"
import horse from "../images/horse.jpeg"

const Form2 = (props) => {

    const {transport, setTransport}= props;

    function handleClick(transport){
        setTransport(transport)
        
            }

    return (
        <div>
        <h1> Votre moyen de locomotion préféré?</h1>
        <div>
            <img onClick= {()=> {handleClick("Vélo")}} src={bike} alt="bike"/>
        </div>
        <div>
        <img onClick= {()=> {handleClick("Bus")}} src={bus} alt="bus"/>
        </div>
        <div>
        <img onClick= {()=> {handleClick("Poney")}} src={horse} alt="horse"/>
        </div>
    </div>
    );
}

export default Form2;