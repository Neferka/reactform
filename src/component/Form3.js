import React from 'react';
const Form3 = (props) => {
    const {voyage, setVoyage}= props;

    function handleClick(event){
let newVoyage= voyage;
let index= newVoyage.indexOf(event.target.value)
if (index===-1){
   newVoyage.push(event.target.value)
}
else {
newVoyage.splice(index,1)
}
setVoyage(newVoyage);


    }
    return (
        <div>
        <h1> Quels sont les prochains pays que vous pensez visiter?</h1>
        <div>
            <input onClick= {handleClick} type="checkbox" id="france" name="france" value="France"/>
            <label htmlFor="france">France</label>
        </div>
        <div>
            <input  onClick= {handleClick} type="checkbox" id="allemagne" name="allemagne" value="Allemagne"/>
            <label htmlFor="allemagne">Allemagne</label>
        </div>
        <div>
            <input  onClick= {handleClick} type="checkbox" id="angleterre" name="angleterre" value="Angleterre"/>
            <label htmlFor="angleterre">Angleterre</label>
        </div>
    </div>
    );
}

export default Form3;