import React from 'react';
const Form1 = (props) => {
    const {origine, setOrigine}= props;

    function handleChange(event){
setOrigine(event.currentTarget.value)

    }

    return (
    <div>
        <h1> D'où venez-vous?</h1>
        <div>
            <input onChange= {handleChange} type="radio" id="france" name="pays" value="France"/>
            <label htmlFor="france">France</label>
        </div>
        <div>
            <input onChange= {handleChange} type="radio" id="allemagne" name="pays" value="Allemagne"/>
            <label htmlFor="allemagne">Allemagne</label>
        </div>
        <div>
            <input onChange= {handleChange} type="radio" id="angleterre" name="pays" value="Angleterre"/>
            <label htmlFor="angleterre">Angleterre</label>
        </div>
    </div>
    );
}

export default Form1;