import React, {useState} from 'react';
const Form4 = (props) => {

    const {info, setInfo}= props;
    
    const[prenom,setPrenom]= useState("")
    const[nom,setNom]= useState("")
    const[adresse,setAdresse]= useState("")
    const[mail,setMail]= useState("")
    const[pays,setPays]= useState("")

    function handlePrenomChange(event){
let newInfo= JSON.parse(JSON.stringify(info));
newInfo.prenom= event.target.value
setInfo(newInfo)
    }

    function handleNomChange(event){
        let newInfo= JSON.parse(JSON.stringify(info));
        newInfo.nom= event.target.value
        setInfo(newInfo)
            }

    function handleAdresseChange(event){
        let newInfo= JSON.parse(JSON.stringify(info));
        newInfo.adresse= event.target.value
        setInfo(newInfo)
                    }

    function handleMailChange(event){
        let newInfo= JSON.parse(JSON.stringify(info));
        newInfo.mail= event.target.value
        setInfo(newInfo)
                    }

    function handlePaysChange(event){
        let newInfo= JSON.parse(JSON.stringify(info));
        newInfo.pays= event.target.value
        setInfo(newInfo)
                }

    return (
        <form>
            <label for="prenom"><b>Prénom</b></label>
            <input onChange= {handlePrenomChange} type="text" placeholder="prénom" name="prenom" id="prenom" required/>

            <label for="nom"><b>Nom</b></label>
            <input onChange= {handleNomChange} type="text" placeholder="Nom" name="nom" id="nom" required/>

                
            <label for="adresse"><b>Adresse</b></label>
            <input onChange= {handleAdresseChange} type="text" placeholder="adresse" name="adresse" id="adresse" required/>

            <label for="email"><b>Email</b></label>
            <input onChange= {handleMailChange} type="email" placeholder="email" name="email" id="email" required/>

            <label for="pays"><b>Pays</b></label>
            <input onChange= {handlePaysChange} type="text" placeholder="pays" name="pays" id="pays" required/>
                            <hr></hr>
    </form>
    );
}

export default Form4;