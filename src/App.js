import React, {useState} from 'react';
import './App.css';
import Form1 from "./component/Form1";
import Form2 from "./component/Form2";
import Form3 from "./component/Form3";
import Form4 from "./component/Form4";
import Form5 from "./component/Form5";
import Footer from "./component/Footer"



function App() {
  const [currentPage,setCurrentPage]= useState(1)
  const[origine,setOrigine]= useState("")
  const[transport,setTransport]= useState("")
  const[voyage,setVoyage]= useState([])
  const[info,setInfo]= useState({})

  
  
  
  

  function selectForm(currentPage){
    if (currentPage === 1){
      return <Form1 origine={origine} setOrigine={setOrigine}/>
    }
    if (currentPage === 2){
      return <Form2 transport={transport} setTransport={setTransport}/>
    }
    if (currentPage === 3){
      return <Form3 voyage={voyage} setVoyage={setVoyage}/>
    }
    if (currentPage === 4){
      return <Form4 info={info} setInfo={setInfo}/>
    }
    if (currentPage === 5){
      return <Form5 origine={origine} transport={transport} voyage={voyage} info={info} />
    }


  }
  return (
    <>
    {selectForm(currentPage)}
    <Footer currentPage= {currentPage} setCurrentPage={setCurrentPage}/>
    </>
  );
}

export default App;
